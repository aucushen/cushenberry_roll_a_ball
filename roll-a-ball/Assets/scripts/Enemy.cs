﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
//mod2
{
    GameObject player;
    Rigidbody rb;
    public float speed;


    
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        rb = GetComponent<Rigidbody>();
        //lf player at start
        
    }

    
    void FixedUpdate()
    {
        Vector3 dirToPlayer = player.transform.position - transform.position;
        dirToPlayer = dirToPlayer.normalized;
        //Ai for following player after movement 

        rb.AddForce(dirToPlayer * speed);
    }

    
        
  
}
