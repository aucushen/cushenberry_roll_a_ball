﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{
    //Declaring vars
    public float speed;
    public Text countText;
    public Text wintext;
    bool onGround = true;
    private Rigidbody rb;
    private int count;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        count = 0;
        SetCountText();
        //wintext.text = ""; //prompt the player to collect balls by using defualt
    }
    //Movement
    void FixedUpdate()
    {
        float moveHorizontal = Input.GetAxis("Horizontal");
        float moveVertical = Input.GetAxis("Vertical");

        Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        rb.AddForce(movement * speed);
    }
    //Spacebar4speed
    void Update()
    {
        onGround = Physics.Raycast(transform.position, Vector3.down, .52f);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            speed = speed * 6f;
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            speed = speed / 6f;          //Mod1
        }
    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Pick Up"))
        {
            other.gameObject.SetActive(false);
            //Created count for endgame
            count = count + 1;
            SetCountText();

        }
    }
    void SetCountText()
    {
        countText.text = "Count  : " + count.ToString();
        if (count >= 12)
        {
            wintext.text = "You Win";
            Invoke("RestartLevel", 2f);
            //Gameover/restart mod3
        }
    }
    void RestartLevel()
  
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    //Restart if enemy hits u
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.other.CompareTag("Enemy"))
        {
            RestartLevel();
        }
    }
    //mod4
}
